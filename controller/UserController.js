const models = require("../models")
const bcrypt = require("bcrypt")
const config = require("../config/app.json")

const index = (req, res) => {
    models.users.findAll().then(users => {
        res.render("index", {
            users: users
        })
    }).catch(err => {
        console.log(err);
        res.end()
    });
};

const add = (req, res) => {
    const password = req.body.password;

    bcrypt.hash(password, config.saltRounds, function(err, hash) {
        models.users.create({
            name: req.body.name,
            email: req.body.email,
            password: hash
        }).then(user => {
            res.send(user)
        }).catch(err => {
            res.send(err)
        })
    });
};

const login = (req, res) => {
    models.users.findOne({
        email: req.body.email
    }).then(user => {
        user.email = "newemail"
        user.save()
        bcrypt.compare(req.body.password, user.password, function(err, result) {
            if (result) {
                res.send(user)
            }
            else {
                res.send("F@ud vr8w00rd")
            }
        });
    }).catch(err => {
        res.send(err)
    })
}

const edit = (req, res) => {
    models.users.findOne({
        id: req.body.id
    }).then(user => {
        user.email = req.body.email;
        user.save().then(r => {
            res.send("New email is set");
        })
    })
}

module.exports = {
    index, add, login
}
