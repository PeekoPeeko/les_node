const express = require("express");
const config = require("./config/app.json");
const site = require("./routes/site");

const app = express();

const port = 8080;

const prefix = config.enviroment === "dev" ? "" : "/~gulian/confrenceAPPP";

app.set("view engine", "ejs");
app.set("views", `${process.cwd()}/resources/views`);

app.use(`${prefix}/`, site);

app.listen(port, () => {
    console.log(`Website on ${port}`);
});
